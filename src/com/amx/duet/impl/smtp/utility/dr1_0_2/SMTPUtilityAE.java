package com.amx.duet.impl.smtp.utility.dr1_0_2;

import com.amx.duet.devicesdk.base.TypeSafeEnumBase;

public  class SMTPUtilityAE extends TypeSafeEnumBase
{
	SMTPUtilityAE(String name)
	{
		super(name);
	}
		
	/**
	 * Feedback on the success of sending email.
	 */
	public static final SMTPUtilityAE EMAIL_SUCCESS = new SMTPUtilityAE("EMAIL_SUCCESS");
	
	
	public String toString()
	{
		return ("[SMTPUtilityAE - " + getName() + "]");
	}
}
