package com.amx.duet.impl.smtp.utility.dr1_0_2;

import java.io.*;
import java.util.*;


import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.*;

import org.osgi.framework.BundleContext;

import com.amx.duet.core.master.netlinx.Event;
import com.amx.duet.da.NetLinxDevice;
import com.amx.duet.devicesdk.Utility;
import com.amx.duet.devicesdk.base.AdvancedEvent;
import com.amx.duet.devicesdk.base.ModuleComponentEvent;


/**
 * @author JMaciejewski
 *
 */


public class SMTPUtility extends Utility{


    private static NetLinxDevice device = null;

	public SMTPUtility() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SMTPUtility(BundleContext bctxt, NetLinxDevice nd, Properties props) {
        super(bctxt, nd, props);
        setProperty("IGNORE-UNKNOWN-NETLINX-COMMAND", "true");
        
//        utilities = new DeviceUtil(this, nd, props);
         device = nd;
        //this.setDebugState(DEBUG);
        // TODO Auto-generated constructor stub
    }

    protected void doAddNetLinxDeviceListeners() {
        // TODO Auto-generated method stub
        
    }

    protected boolean doNetLinxDeviceInitialization() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isDeviceOnLine() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isDataInitialized() {
        // TODO Auto-generated method stub
        return false;
    }
    
    public void handleDataEvent(Event e, int i) {
    	// No handle event errors
    }
    
	public void handleCommandEvent(Event e, String cmd) {
	
        log(DEBUG,"got cmd: " + cmd);
        //SMTP_SEND-jim.maciejewski@example.com.au,This is my Subject,This is my body, NULL_STR
        try {          
            if(cmd.startsWith("SMTP_SEND-")) {
                log(DEBUG,"in send: " + cmd);
                String[] details = cmd.split(",");
                String to = details[0].replace("SMTP_SEND-", "");
                String subject = details[1];
                String body = details[2];
                String filePath = "NULL_STR";
                if(details.length == 4) {
                	filePath = details[3];
                }
                sendEmail(to, subject, body, filePath);                         
            }
        }
        catch (Exception exc) {
        	log(DEBUG, exc.toString());
        	sendFeedback("SMTP_SEND-ERROR,Unable to process arguments");  	
        }
    }
	

    public void sendEmail(String to, String subject, String body, String filePath) {
      log(DEBUG,"In sendEmail");
      final String smtp_address = super.getProperty("SMTP_ADDRESS");
      final String port = super.getProperty("PORT_NUMBER");
      final String user = super.getProperty("USERNAME");
      final String pass = super.getProperty("PASSWORD");
      final String from = super.getProperty("FROM");
      final String validate = super.getProperty("VALIDATE_CERTIFICATE");

      

      log(DEBUG,"Current Configuration values:");
      log(DEBUG,"smtp_address: " + smtp_address);
      log(DEBUG,"port: " + port);
      log(DEBUG,"validate: " + validate);

      log(DEBUG, "Call values:");
      log(DEBUG,"to: " + to);
      log(DEBUG,"subject: " + subject);
      log(DEBUG,"body: " + body);
      log(DEBUG,"filePath: " + filePath);

      try 
      {
          Properties props = new Properties();
          props.put("mail.smtp.auth", "true");
          props.put("mail.smtp.starttls.enable", "true");
          props.put("mail.smtp.host", smtp_address);
          props.put("mail.smtp.port", port);
          if(validate.equals("false")) {
              props.put("mail.smtp.ssl.trust", "*");
          }

          Thread.currentThread().setContextClassLoader( getClass().getClassLoader() );
          MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
          mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
          mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
          mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
          mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
          mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
          
          Session session = Session.getInstance(props, null);
          MimeMessage message = new MimeMessage(session);

          message.setFrom(new InternetAddress(from));
          message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to.replace(';', ',')));
          message.setSubject(subject);
          //message.setText(body);
          
          // creates message part
          MimeBodyPart messageBodyPart = new MimeBodyPart();
          messageBodyPart.setText(body);
   
          // creates multi-part
          Multipart multipart = new MimeMultipart();
          multipart.addBodyPart(messageBodyPart);
   
          // adds attachments
          if (filePath.equals("NULL_STR")) {
        	  log(DEBUG,"No attachment.");
          } else {
        	  log(DEBUG,"Adding attachment.");
              MimeBodyPart attachFile = new MimeBodyPart();
  
              try {
                  attachFile.attachFile(filePath);
                  multipart.addBodyPart(attachFile);
              } catch (IOException ex) {
            	  log(DEBUG,ex.toString());
              }
          }
   
          // sets the multi-part as e-mail's content
          message.setContent(multipart);
   
          // sends the e-mail
          
          message.saveChanges();
          
          Transport transport = session.getTransport("smtp");
          transport.connect(smtp_address, user, pass); 
          transport.sendMessage(message, message.getAllRecipients());
          transport.close();
          
          // Send feed back
	      log(DEBUG,"sending feedback message");
	      sendFeedback("SMTP_SEND-SUCCESS,Email Sent");
          
          
          
      }
      catch (AddressException e) {
    	  log(DEBUG,e.toString());
    	  sendFeedback("SMTP_SEND-ERROR,Address Exception,Email failed to send");
    	  
    	  }
      catch (MessagingException e) {
    	  log(DEBUG,e.toString());
    	  sendFeedback("SMTP_SEND-ERROR,Messaging Exception,Email failed to send");
    	  }
    }
    
    public void sendFeedback(String msg) {
		AdvancedEvent advEv = new AdvancedEvent("Success", SMTPUtilityAE.EMAIL_SUCCESS);
//		com.amx.duet.core.master.netlinx.DPS dps = SMTPUtility.device.getDPS();
		Event e = new Event();
		e.dps = SMTPUtility.device.getDPS();
		e.type = Event.E_COMMAND;
		e.dataType = Event.D_STRING8;
		e.dataValue = msg.getBytes();
		this.processAdvancedEvent(new ModuleComponentEvent(this, advEv, e, 1));
    }

    public void refresh() {
        // TODO Auto-generated method stub
    super.refresh();
    }

    public void reinitialize() {
        // TODO Auto-generated method stub
    super.reinitialize();
    }

    public void dispose() {
        // TODO Auto-generated method stub
    super.dispose();
    }

}
