# Netlinx SMTP Utility Duet Module

This tool is a workaround for AMX NX built in smtp_send command which only supports TLSv1.0

By loading this module you will be able to send emails using TLSv1.2

## To Use

In your Netlinx code you will need to initialize the SMTP server settings,
I recommend doing this during the SMTP Module online event:
    
```JavaScript
DATA_EVENT[vdvSMTP]
{
    ONLINE:
    {
        send_command vdvSMTP,"'PROPERTY-SMTP_ADDRESS,smtp.example.com.au'"
        send_command vdvSMTP,"'PROPERTY-PORT_NUMBER,587'"
        send_command vdvSMTP,"'PROPERTY-USERNAME,smtp_user@example.com.au'"
        send_command vdvSMTP,"'PROPERTY-PASSWORD,password'"
        send_command vdvSMTP,"'PROPERTY-FROM,jim.maciejewski@example.com.au'"
        send_command vdvSMTP,"'PROPERTY-VALIDATE_CERTIFICATE,true'"
    }
}
```


After setting the SMTP server values, you can send an email like this:


```JavaScript
send_command vdvSMTP,"'SMTP_SEND-customer.name@example.com.au,This is the Subject,This is the body,NULL_STR'"
```


### Notes

Tested with NX firmware v1.6.179, v1.6.175

You will need to add the SMTPUtility_dr1_0_2.jar and javax.mail.1.6.jar to your Netlinx project modules, these are located in the OUTPUT folder in this repository. 

Send the command DEBUG-4 to the duet modules virtual device to get debugging messages.

TO_ADDRESS - a string containing the email address of the destination. String must be less than 127 characters.

SUBJECT - a string containing the email subject line

BODY - a string containing the email body text

TEXT_ATTACHMENT - a string containing the filename of a text file to be attached to the email.  

* Filename must be less than 256 characters and file size must be under 65536 bytes.  

* Can be left blank or set to NULL_STR when no attachment is desired.

### Additional features

You can now send to multiple recipients by add email addresses separated by semicolons, for example:

```JavaScript
send_command vdvSMTP,"'SMTP_SEND-customer1@example.com.au;customer2@example.com.au,This is the Subject,This is the body,attachment.txt'"
```


### Known issues

This duet module will not work with NX firmware v1.6.184, but does work on v1.6.193.

### Example Netlinx code

---
```JavaScript
PROGRAM_NAME='MAIN'
(***********************************************************)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 24/02/2021  AT: 15:41:25        *)
(***********************************************************)
(* System Type : NetLinx                                   *)
(***********************************************************)
(* REV HISTORY:                                            *)
(***********************************************************)
(*
    $History: $
*)
(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

vdvSMTP = 41001:1:0   // Virtual Device for Duet Module
dvSocket = 0:2:0      // Network port for Duet Module
dvTP = 10001:1:0      // Touchpanel for testing


DEFINE_MODULE

'SMTPUtility_dr1_0_2' smtp(vdvSMTP, dvSocket)   // define module and pass virtual device, socket

DEFINE_EVENT

DATA_EVENT[vdvSMTP]
{
    ONLINE:
    {
        // When the duet module comes online, set all the required properties
        send_command vdvSMTP,"'PROPERTY-SMTP_ADDRESS,smtp.example.com.au'"
        send_command vdvSMTP,"'PROPERTY-PORT_NUMBER,587'"
        send_command vdvSMTP,"'PROPERTY-USERNAME,smtp_user@example.com.au'"
        send_command vdvSMTP,"'PROPERTY-PASSWORD,password'"
        send_command vdvSMTP,"'PROPERTY-FROM,jim.maciejewski@example.com.au'"
        send_command vdvSMTP,"'PROPERTY-VALIDATE_CERTIFICATE,true'"
    }
}


BUTTON_EVENT[dvTP,100]
{
    PUSH:
    {
        send_command vdvSMTP,"'SMTP_SEND-customer.name@example.com.au,This is the Subject,This is the body,attachment.txt'"
    }
}
```
